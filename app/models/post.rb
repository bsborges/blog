class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy #destroy all comments associated with a specific post
  validates_presence_of :title
  validates_presence_of :body
end
